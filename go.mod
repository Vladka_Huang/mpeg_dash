module mpeg

go 1.14

require (
	github.com/biezhi/gorm-paginator/pagination v0.0.0-20190124091837-7a5c8ed20334
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/size v0.0.0-20200815104238-dc717522c4e2
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.4.2
	github.com/gomodule/redigo/redis v0.0.0-20200429221454-e14091dffc1b
	github.com/google/uuid v1.1.2
	github.com/jinzhu/gorm v1.9.15
	github.com/magiconair/properties v1.8.1
	github.com/speps/go-hashids v2.0.0+incompatible
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4
	google.golang.org/grpc v1.31.1
	google.golang.org/protobuf v1.23.0
	gopkg.in/olahol/melody.v1 v1.0.0-20170518105555-d52139073376
)
