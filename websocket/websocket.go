package websocket

import (
	"encoding/json"
	"gopkg.in/olahol/melody.v1"
	"log"
	"sync"
)

type Struct struct {
	Melody  *melody.Melody
	JobChan chan []byte
}

type CommentData struct {
	Name string `json:"name" binding:"required"`
	Content string `json:"content" binding:"required"`
	Start float64 `json:"start" binding:"omitempty,gte=0"`
}

type CommentEvent struct {
	VideoId string `json:"videoId"`
	Data CommentData `json:"data"`
}

var mutex sync.RWMutex
var sessionMap map[string]map[*melody.Session]bool = make(map[string]map[*melody.Session]bool)
var CloseChan chan *melody.Session = make(chan *melody.Session, 100)

var Ws Struct

func New() {
	Ws.Melody = melody.New()
	Ws.JobChan = make(chan []byte)
}

func Dail() {
	Ws.Melody.HandleConnect(Connect)
	Ws.Melody.HandleMessage(Message)
	Ws.Melody.HandleDisconnect(Disconnect)

	go func() {
		for {
			select {
			case s := <-CloseChan:
				go func() {
					if !s.IsClosed() {
						_ = s.Close()
					}
					room := s.Request.FormValue("room")
					mutex.Lock()
					delete(sessionMap[room], s)
					mutex.Unlock()
				}()

			case job := <-Ws.JobChan:
				var event CommentEvent
				if err := json.Unmarshal(job, &event); err != nil {
					log.Println(err)
				}
				sessions := make([]*melody.Session, 0, len(sessionMap[event.VideoId]))
				for session, _ := range sessionMap[event.VideoId] {
					sessions = append(sessions, session)
				}
				if err := Ws.Melody.BroadcastMultiple(job, sessions); err != nil {
					log.Println(err)
				}
			}
		}
	}()
}

func Connect(session *melody.Session) {
	room := session.Request.FormValue("room")
	log.Println("Websocket new connection room : ", room)

	mutex.Lock()
	if sessionMap[room] == nil {
		sessionMap[room] = make(map[*melody.Session]bool)
	}
	sessionMap[room][session] = true
	mutex.Unlock()
}

func Message(session *melody.Session, msg []byte) {
	log.Println("Receive message: ", msg)
	if err := session.Write(msg); err != nil {
		log.Println(err)
	}
}

func Disconnect(session *melody.Session) {
	log.Println("Websocket disconnect session: ", session)
	CloseChan <- session
}