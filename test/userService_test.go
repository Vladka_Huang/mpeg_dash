
package services

import (
	"errors"
	"github.com/biezhi/gorm-paginator/pagination"
	"github.com/magiconair/properties/assert"
	"mpeg/app/models"
	"mpeg/app/services"
	"mpeg/mocks"
	"testing"
	"time"
)

var mockUser = mocks.UserRepository{}
var userService *services.UserService

func init() {
	userService = services.NewUserService(&mockUser)
}

func getFakeUserList(size int) []models.User{
	users := make([]models.User, size, size)

	for i := 1; i < 5; i++ {
		user := models.User{
			ID:        int64(i),
			Account:   "",
			Name:      "",
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
		}
		users = append(users, user)
	}

	return users
}

type userOutput struct {
	Id         string
	Account    string `json:"account"`
	Name       string `json:"name"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

func getEncodedUserList(users []models.User) []*userOutput{
	var usersOutput []*userOutput

	for _, val := range users {
		val.GenerateHashId()
		tmp := &userOutput{
			Id:        val.GetHashId(),
			Account:   val.Account,
			Name:      val.Name,
			CreatedAt: val.CreatedAt.String(),
			UpdatedAt: val.UpdatedAt.String(),
		}
		usersOutput = append(usersOutput, tmp)
	}

	return usersOutput
}

func TestUserService_GetList(t *testing.T) {
	page := 1

	size := 5
	users := getFakeUserList(size)
	paginator := &pagination.Paginator{
		TotalRecord: size,
		TotalPage:   1,
		Records:     &users,
		Offset:      0,
		Limit:       10,
		Page:        1,
		PrevPage:    1,
		NextPage:    2,
	}
	mockUser.On("GetList", page).Return(paginator).Once()

	actual := userService.GetList(page)

	paginator.Records = getEncodedUserList(users)
	expect := paginator

	mockUser.AssertExpectations(t)
	assert.Equal(t, actual, expect)
}

func TestUserService_LoginFail(t *testing.T) {
	mockUser.On("GetPassword").Return("serdtfgyuhijokpl").Once()
	actual, err := userService.Login("")
	mockUser.AssertExpectations(t)
	assert.Equal(t, actual, "")
	assert.Equal(t, err, errors.New("login failed"))
}

func TestUserService_GetInfo(t *testing.T) {
	reqUser := models.User{}
	mockUser.On("GenerateHashId").Return(nil).Once()
	userService.GetInfo(reqUser)
	mockUser.AssertExpectations(t)
}