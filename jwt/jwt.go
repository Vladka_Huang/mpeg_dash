package jwt

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
	"time"
)

var jwtKey = []byte(viper.GetString("app_key"))

type Claims struct {
	UserId uint
	jwt.StandardClaims
}

type ServerClaims struct {
	jwt.StandardClaims
	ServerName string `json:"serverName"`
}

func TokenGenerate(id uint) (string, error) {
	expiredAt := time.Now().Add(24 * time.Hour)
	claims := &Claims{
		UserId: id,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiredAt.Unix(),
			IssuedAt:  time.Now().Unix(),
			Subject:   "user token",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	if tokenString, err := token.SignedString(jwtKey); err != nil {
		return "", err
	} else {
		return tokenString, nil
	}
}

func ParseToken(tokenString string) (*jwt.Token, *Claims, error) {
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	return token, claims, err
}

func ServerTokenGenerate(serverName string) (string, error) {
	expiredAt := time.Now().Add(30 * 24 * time.Hour)
	claims := &ServerClaims{
		ServerName: serverName,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiredAt.Unix(),
			IssuedAt:  time.Now().Unix(),
			Subject:   "server token",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	if tokenString, err := token.SignedString(jwtKey); err != nil {
		return "", err
	} else {
		return tokenString, nil
	}
}

func ParseServerToken(tokenString string) (*jwt.Token, *ServerClaims, error) {
	claims := &ServerClaims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	return token, claims, err
}

