package migrations

import (
	"github.com/jinzhu/gorm"
)

type Comment struct {
	gorm.Model
	VideoId uint
	Name string `gorm:"size:255"`
	Content string `gorm:"size:255"`
	Start float64
}