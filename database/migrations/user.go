package migrations

import "github.com/jinzhu/gorm"

type User struct {
	gorm.Model
	Account string
	Password string
	Name string `gorm:"size:255"`
}