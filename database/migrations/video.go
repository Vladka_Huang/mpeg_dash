package migrations

import "github.com/jinzhu/gorm"

type Video struct {
	gorm.Model
	UserId uint
	Title string `gorm:"size:255"`
	Name string `gorm:"size:255"`
	IsDash bool	`gorm:"default:false"`
}