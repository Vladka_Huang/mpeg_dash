package migrations

import (
	"fmt"
	"mpeg/database"
)

func Migrate() {
	fmt.Println("Migrating.....")
	database.Db.AutoMigrate(&Video{}, &User{}, &Comment{})

	fmt.Println("Migrated")
}