package database

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"mpeg/config"
)

var Db *gorm.DB

func Open() (err error) {
	config.LoadDb()
	dbConfig := config.DbSetting
	driver := dbConfig.Driver
	dsn := fmt.Sprintf("%s:%s@(%s:%d)/%s?parseTime=true",
		dbConfig.User, dbConfig.Password, dbConfig.Host, dbConfig.Port, dbConfig.Database)
	Db, err = gorm.Open(driver, dsn)
	return
}

func Close() error{
	return Db.Close()
}