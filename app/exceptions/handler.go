package exceptions

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Validation(c *gin.Context, err error){
	c.JSON(http.StatusUnprocessableEntity, gin.H{
		"message": err.Error(),
	})
	return
}

func RequestInvalid(c *gin.Context, err error){
	c.JSON(http.StatusBadRequest, gin.H{
		"code": 400,
		"message": err.Error(),
	})
}

func Unauthorized(c *gin.Context, msg string)  {
	c.JSON(http.StatusUnauthorized, gin.H{
		"code": 401,
		"message": msg,
	})
}

func Custom(c *gin.Context, msg string, code int) {
	c.JSON(code, gin.H{
		"code": code,
		"message": msg,
	})
}