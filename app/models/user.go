package models

import (
	"github.com/biezhi/gorm-paginator/pagination"
	"mpeg/app/library"
	orm "mpeg/database"
	"time"
)

type UserRepository interface {
	GetList(int) *pagination.Paginator
	Create(string, string, string) error
	GetByAccount() error
	Find(uint) error
	GenerateHashId()
	GetHashId() string
	GetId() int64
	GetPassword() string
	GetAccount() string
	GetName() string
}

type User struct {
	ID int64 `json:"id"`
	Account string `json:"account"`
	Password string `json:"password"`
	Name string `json:"name"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
	hashId string
}

func (user *User)GetList(page int) *pagination.Paginator {
	var users []User
	db := orm.Db.Select("id, account, name, created_at, updated_at")
	return pagination.Paging(&pagination.Param{
		DB:      db,
		Page:    page,
		Limit:   10,
		OrderBy: []string{"id desc"},
		ShowSQL: false,
	}, &users)
}

func (user *User)Create(account string, password string, name string) (err error) {
	if err = orm.Db.Create(&user).Error; err != nil {
		return
	}
	return
}

func (user *User)GetByAccount() (err error) {
	err = orm.Db.Where(&user).First(&user).Error
	return
}

func (user *User)Find(id uint) error {
	db := orm.Db.First(&user, id)
	if err := db.Error; err != nil {
		return err
	}
	return nil
}

func (user *User)GenerateHashId() {
	user.hashId,_ = library.GetHashID("User").Encode([]int{int(user.ID)})
}

func (user *User)GetHashId() string {
	return user.hashId
}

func (user *User)GetId() int64 {
	return user.ID
}

func (user *User)GetPassword() string {
	return user.Password
}

func (user *User) GetAccount() string {
	return user.Account
}

func (user *User) GetName() string {
	return user.Name
}
