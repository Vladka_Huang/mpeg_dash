package models

import (
	"github.com/biezhi/gorm-paginator/pagination"
	"mpeg/app/library"
	orm "mpeg/database"
	"time"
)

type VideoRepository interface {
	Create() error
	GetList(int) *pagination.Paginator
	GetUserVideos(int64, int) *pagination.Paginator
	GenerateHashId()
	GenerateUserHashId()
	GetHashId() string
	GetUserHashId() string
	FindByHashId(string) error
	GetComments() ([]Comment, error)
	Formatted(string) error
	IsFormatted() bool
	GetName() string
}

type Video struct {
	ID int64 `json:"id"`
	UserId int64 `json:"user_id"`
	Title string `json:"title"`
	Name string
	IsDash bool
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
	hashId string
	userHashId string
}

func (video *Video)Create() (err error) {
	if err = orm.Db.Create(&video).Error; err != nil {
		return
	}
	return
}

func (video *Video)GetList(page int) *pagination.Paginator {
	var videos []Video
	db := orm.Db
	return pagination.Paging(&pagination.Param{
		DB:      db,
		Page:    page,
		Limit:   6,
		OrderBy: []string{"id desc"},
		ShowSQL: false,
	}, &videos)
}

func (video *Video)GetUserVideos(userId int64, page int) *pagination.Paginator {
	var videos []Video
	db := orm.Db.Where("user_id = ?", userId)
	return pagination.Paging(&pagination.Param{
		DB:      db,
		Page:    page,
		Limit:   10,
		OrderBy: []string{"id desc"},
		ShowSQL: false,
	}, &videos)
}

func (video *Video)GenerateHashId() {
	video.hashId,_ = library.GetHashID("Video").Encode([]int{int(video.ID)})
}

func (video *Video)GenerateUserHashId() {
	video.userHashId,_ = library.GetHashID("User").Encode([]int{int(video.UserId)})
}

func (video *Video)GetHashId() string{
	return video.hashId
}

func (video *Video)GetUserHashId() string{
	return video.userHashId
}

func (video *Video)FindByHashId(hashId string) error {
	id, err := library.GetHashID("Video").DecodeWithError(hashId)
	if err != nil {
		return err
	}
	db := orm.Db.First(&video, id[0])
	if err := db.Error; err != nil {
		return err
	}
	return nil
}

func (video *Video)GetComments() ([]Comment, error) {
	var comments []Comment
	err := orm.Db.Where("video_id = ?", video.ID).Order("start").Find(&comments).Error
	if err != nil {
		return nil, err
	}
	return comments, nil
}

func (video *Video)Formatted(name string) error {
	if err := orm.Db.Model(&video).Where("name = ?", name).Update("is_dash", true).Error; err != nil {
		return err
	}
	return nil
}

func (video *Video) IsFormatted() bool {
	return video.IsDash
}

func (video *Video) GetName() string {
	return video.Name
}
