package models

import (
	"mpeg/app/library"
	orm "mpeg/database"
	"time"
)

type Comment struct {
	ID int64 `json:"id"`
	VideoId int64 `json:"video_id"`
	Content string `json:"title"`
	Name string `json:"name"`
	Start float64 `json:"start"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`
	hashId string
	videoHashId string
}

func (comment *Comment)Create() (err error) {
	if err = orm.Db.Create(&comment).Error; err != nil {
		return
	}
	return
}

func (comment *Comment)GenerateHashId() {
	comment.hashId,_ = library.GetHashID("Comment").Encode([]int{int(comment.ID)})
}

func (comment *Comment)GetHashId() string {
	return comment.hashId
}