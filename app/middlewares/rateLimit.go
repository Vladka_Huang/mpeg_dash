package middlewares

import (
	"github.com/gin-gonic/gin"
	"golang.org/x/time/rate"
	"mpeg/app/exceptions"
)

func RateAllow(limit *rate.Limiter) gin.HandlerFunc {
	return func(c *gin.Context) {
		if !limit.Allow() {
			exceptions.Unauthorized(c, "rate limit")
			c.Abort()
			return
		}
		c.Next()
	}
}