package middlewares

import (
	"github.com/gin-gonic/gin"
	redigo "github.com/gomodule/redigo/redis"
	"log"
	"mpeg/app/exceptions"
	"mpeg/app/models"
	"mpeg/jwt"
	"mpeg/redis"
	"strings"
)

func UserAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
			tokenString := c.GetHeader("Authorization")
			if tokenString == "" || !strings.HasPrefix(tokenString, "Bearer ") {
				exceptions.Unauthorized(c, "token prefix error")
				c.Abort()
				return
			}

			tokenString = tokenString[7:]
			if inBlacklist(tokenString) {
				exceptions.Unauthorized(c, "token invalid")
				c.Abort()
				return
			}

			token, claims, err := jwt.ParseToken(tokenString)
			if err != nil || !token.Valid {
				exceptions.Unauthorized(c, "token invalid")
				c.Abort()
				return
			}

			userId := claims.UserId
			var user models.User

			if err := user.Find(userId); err != nil{
				exceptions.Unauthorized(c, "user not found")
				c.Abort()
				return
			}

			c.Set("user", user)
			c.Set("token", tokenString)
			c.Next()
		}
}

func inBlacklist(token string) bool {
	redisConn := redis.Pool.Get()
	defer redisConn.Close()

	if result, err := redigo.Bool(redisConn.Do("EXISTS", token)); err != nil || result {
		log.Println(err)
		return true
	}

	return false
}