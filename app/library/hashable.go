package library

import (
	"github.com/speps/go-hashids"
	"golang.org/x/crypto/bcrypt"
	"log"
	"mpeg/config"
	"reflect"
)

var hashConf *config.HashConf
var HashIDData *hashids.HashIDData
var HashIds map[string]*hashids.HashID

func HashPassword(password string) (string, error) {
	if hashedPwd, err := bcrypt.GenerateFromPassword([]byte(password), 17); err != nil {
		log.Println(err)
		return "", err
	} else {
		return string(hashedPwd), nil
	}
}

func VerifyPassword(hashedPwd string, plainPwd string) bool {
	//fmt.Println("Start Compare: ", time.Now().Format("2006-01-02 15:04:05.000000"))
	if err := bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(plainPwd)); err != nil {
		return false
	}
	//fmt.Println("Completed Compare: ", time.Now().Format("2006-01-02 15:04:05.000000"))
	return true
}

func getFieldString(e *config.HashConf, field string) string {
	r := reflect.ValueOf(e)
	f := reflect.Indirect(r).FieldByName(field)
	if f.IsValid() {
		return f.String()
	}
	return ""
}

func GetHashID(model string) *hashids.HashID {
	if HashIDData == nil {
		config.LoadHashIds()
		hashConf = config.HashConfs
		HashIDData = hashids.NewData()
		HashIDData.MinLength = 10
		HashIds = make(map[string]*hashids.HashID)
	}

	if hashId, ok := HashIds[model]; ok {
		return hashId
	}

	if salt := getFieldString(hashConf, model); salt != "" {
		HashIDData.Salt = salt
		hashId, _ := hashids.NewWithData(HashIDData)
		HashIds[model] = hashId
		return hashId
	}

	return nil
}