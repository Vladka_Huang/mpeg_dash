package controllers

import (
	"github.com/gin-gonic/gin"
	"log"
	"mpeg/app/exceptions"
	"mpeg/app/models"
	"mpeg/app/services"
	"mpeg/redis"
	"net/http"
	"strconv"
)

func GetUserList(c *gin.Context) {
	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	var user models.User

	userService := services.NewUserService(&user)
	paginator := userService.GetList(page)

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
		"data": paginator,
	})
}

type createData struct {
	Account string `form:"account" json:"account" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
	Name string `form:"name" json:"name" binding:"required"`
}

func Create(c *gin.Context) {
	var data createData
	if err := c.ShouldBindJSON(&data); err != nil {
		exceptions.Validation(c, err)
		return
	}

	var user models.User
	userService := services.NewUserService(&user)

	if foundUser, _ := userService.FindUser(data.Account); foundUser.GetId() != 0 {
		exceptions.Custom(c, "account existed", 999)
		return
	}

	hashId, err := userService.Create(data.Account, data.Password, data.Name)
	if err != nil {
		exceptions.RequestInvalid(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
		"data": hashId,
	})
}

type loginData struct {
	Account string `form:"account" json:"account" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

func Login(c *gin.Context) {
	var data loginData
	if err := c.ShouldBindJSON(&data); err != nil {
		exceptions.Validation(c, err)
		return
	}

	var user models.User
	userService := services.NewUserService(&user)

	if _, err := userService.FindUser(data.Account); err != nil {
		exceptions.RequestInvalid(c, err)
		return
	}

	token, err := userService.Login(data.Password)
	if err != nil {
		exceptions.RequestInvalid(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
		"data": token,
	})
}

func GetInfo(c *gin.Context) {
	reqUser,_ := c.Get("user")
	userService := services.NewUserService(&models.User{})
	user := userService.GetInfo(reqUser)

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
		"data": gin.H{
			"id": user.GetHashId(),
			"account": user.GetAccount(),
			"name": user.GetName(),
		},
	})
}

func Logout(c *gin.Context) {
	redisConn := redis.Pool.Get()
	defer redisConn.Close()

	token,_ := c.Get("token")
	if _, err := redisConn.Do("SETEX", token, 86400,"value"); err != nil {
		log.Println(err)
		exceptions.RequestInvalid(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
	})
}