package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"log"
	"mpeg/app/exceptions"
	"mpeg/app/models"
	"mpeg/websocket"
	"net/http"
)

func sendEvent(comment websocket.CommentEvent) {
	bytes, _ := json.Marshal(comment)
	log.Println("Set Job : ", comment)
	websocket.Ws.JobChan <- bytes
}

func CreateComment(c *gin.Context) {
	var data websocket.CommentData
	videoId := c.Param("videoId")
	if err := c.ShouldBindJSON(&data); err != nil {
		exceptions.Validation(c, err)
		return
	}

	video := &models.Video{}
	if err := video.FindByHashId(videoId); err != nil {
		exceptions.Custom(c, "model not found", 404)
		return
	}

	comment := models.Comment{
		VideoId: video.ID,
		Content: data.Content,
		Name:    data.Name,
		Start:   data.Start,
	}
	if err := comment.Create(); err != nil {
		exceptions.RequestInvalid(c, err)
		return
	}
	comment.GenerateHashId()
	sendEvent(websocket.CommentEvent{
		VideoId: videoId,
		Data: data,
	})

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
		"data": comment.GetHashId(),
	})
}

func GetComments(c *gin.Context) {

	videoId := c.Param("videoId")
	video := &models.Video{}
	if err := video.FindByHashId(videoId); err != nil {
		exceptions.Custom(c, "model not found", 404)
		return
	}

	comments, err := video.GetComments()
	if err != nil {
		exceptions.RequestInvalid(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
		"data": comments,
	})

}