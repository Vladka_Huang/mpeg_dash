package controllers

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"mpeg/app/exceptions"
	"mpeg/app/models"
	"mpeg/app/services"
	"mpeg/grpc_client"
	pb "mpeg/pb"
	"net/http"
	"strconv"
)

type videoOutput struct {
	Id        string
	UserId    string
	Title     string
	CreatedAt string
	UpdatedAt string
}

func UploadVideo(c *gin.Context) {
	file, fileHeader, err := c.Request.FormFile("video")
	if err != nil {
		exceptions.RequestInvalid(c, err)
		return
	}

	fileName := fileHeader.Filename
	title := c.PostForm("title")
	if len(title) == 0 {
		title = fileName
	}
	if len(title) > 255 {
		title = string([]rune(title)[:255])
	}

	reqUser, _ := c.Get("user")
	user, _ := reqUser.(models.User)
	user.GenerateHashId()

	videoName, err := grpc_client.UploadVideo(grpc_client.Client, file, fileName, user.GetHashId())

	if err != nil {
		exceptions.RequestInvalid(c, err)
		return
	}

	video := models.Video{
		UserId: user.ID,
		Title:  title,
		Name:   videoName,
	}

	if err := video.Create(); err != nil {
		exceptions.RequestInvalid(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
	})
}

func GetUploadedVideos(c *gin.Context) {
	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	reqUser, _ := c.Get("user")
	user, _ := reqUser.(models.User)

	videoService := services.NewVideoService(&models.Video{})
	paginator := videoService.GetUserVideos(user.ID, page)

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
		"data": paginator,
	})
}

func GetVideoList(c *gin.Context) {
	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	videoService := services.NewVideoService(&models.Video{})
	paginator := videoService.GetVideoList(page)

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
		"data": paginator,
	})
}

func Formatted(c *gin.Context) {
	name := c.Query("name")
	videoService := services.NewVideoService(&models.Video{})
	if err := videoService.Formatted(name); err != nil {
		exceptions.RequestInvalid(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
	})
}

func GetPath(c *gin.Context) {
	videoId := c.Param("videoId")
	videoService := services.NewVideoService(&models.Video{})
	path, err := videoService.GetPath(videoId)
	if err != nil {
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": "0000",
		"data": path,
	})
}

func Stream(c *gin.Context) {
	videoId := c.Param("videoId")
	videoService := services.NewVideoService(&models.Video{})
	path, err := videoService.GetPath(videoId)
	if err != nil {
		return
	}

	dataChan := make(chan *pb.ServeVideoResponse)
	res := bytes.NewBuffer([]byte{})

	var contentType string
	file := c.Param("segment")

	if file == "mpd" {
		path = path + "dash.mpd"
		contentType = "text/xml; charset=utf-8"

		c.Header("Accept-Ranges", "bytes")
		c.Data(200, "text/xml; charset=utf-8", res.Bytes())

	} else {
		path = path + file
		contentType = "video/mp4"
	}

	go func() {
		err := grpc_client.ServeVideo(grpc_client.Client, path, dataChan)
		if err != nil {
			exceptions.RequestInvalid(c, err)
			return
		}
	}()

	for msg := range dataChan {
		res.Write(msg.File.FileData)
	}
	c.Data(200, contentType, res.Bytes())
}
