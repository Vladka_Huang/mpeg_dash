package services

import (
	"github.com/biezhi/gorm-paginator/pagination"
	"mpeg/app/models"
)

type VideoService struct {
	video models.VideoRepository
}

type videoOutput struct {
	Id        string
	UserId    string
	Title     string
	CreatedAt string
	UpdatedAt string
}

func NewVideoService(video models.VideoRepository) *VideoService {
	return &VideoService{
		video: video,
	}
}

func (s *VideoService) GetUserVideos(userId int64, page int) (paginator *pagination.Paginator) {
	paginator = s.video.GetUserVideos(userId, page)
	transHashIds(paginator)
	return
}

func (s *VideoService) GetVideoList(page int) (paginator *pagination.Paginator) {
	paginator = s.video.GetList(page)
	transHashIds(paginator)
	return
}

func transHashIds(paginator *pagination.Paginator) {
	var videoList []*videoOutput

	records := *paginator.Records.(*[]models.Video)
	for _, val := range records {
		val.GenerateHashId()
		val.GenerateUserHashId()
		tmp := &videoOutput{
			Id:        val.GetHashId(),
			UserId:    val.GetUserHashId(),
			Title:     val.Title,
			CreatedAt: val.CreatedAt.String(),
			UpdatedAt: val.UpdatedAt.String(),
		}
		videoList = append(videoList, tmp)
	}
	paginator.Records = videoList
}

func (s *VideoService) Formatted (name string) (err error) {
	err = s.video.Formatted(name)
	return
}

func (s *VideoService) GetPath(videoId string) (path string, err error) {
	if err = s.video.FindByHashId(videoId); err != nil {
		return
	}
	s.video.GenerateUserHashId()
	if s.video.IsFormatted() {
		path = s.video.GetUserHashId() + "/" + s.video.GetName() + "/dash/"
	} else {
		path = s.video.GetUserHashId() + "/" + s.video.GetName()
	}
	return
}