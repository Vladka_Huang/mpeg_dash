package services

import (
	"errors"
	"fmt"
	"github.com/biezhi/gorm-paginator/pagination"
	"mpeg/app/library"
	"mpeg/app/models"
	"mpeg/jwt"
	"os"
)

type UserService struct {
	user models.UserRepository
}

func NewUserService(user models.UserRepository) *UserService {
	return &UserService{
		user: user,
	}
}

func (s *UserService) GetList(page int) *pagination.Paginator {
	paginator := s.user.GetList(page)
	records := *paginator.Records.(*[]models.User)

	type userOutput struct {
		Id         string
		Account    string `json:"account"`
		Name       string `json:"name"`
		CreatedAt string `json:"created_at"`
		UpdatedAt string `json:"updated_at"`
	}
	var users []*userOutput

	for _, val := range records {
		val.GenerateHashId()
		tmp := &userOutput{
			Id:        val.GetHashId(),
			Account:   val.Account,
			Name:      val.Name,
			CreatedAt: val.CreatedAt.String(),
			UpdatedAt: val.UpdatedAt.String(),
		}
		users = append(users, tmp)
	}
	paginator.Records = users

	return paginator
}

func (s *UserService) Create(account string, password string, name string) (string, error) {
	password, err := library.HashPassword(password)
	if err != nil {
		return "", err
	}

	if err := s.user.Create(account, password, name); err != nil {
		return "", err
	}

	s.user.GenerateHashId()
	videoPath := fmt.Sprintf("storage/videos/%s", s.user.GetHashId())
	if err := os.Mkdir(videoPath, 0775); err != nil {
		return "", err
	}

	return s.user.GetHashId(), nil
}

func (s *UserService) FindUser(account string) (models.UserRepository, error) {
	s.user = &models.User{
		Account:  account,
	}
	err := s.user.GetByAccount()
	return s.user, err
}

func (s *UserService) Login(password string) (string, error) {
	if !library.VerifyPassword(s.user.GetPassword(), password) {
		return "", errors.New("login failed")
	}

	token, err := jwt.TokenGenerate(uint(s.user.GetId()))
	return token, err
}

func (s *UserService) GetInfo(reqUser interface{}) models.UserRepository {
	user,_ := reqUser.(models.User)
	s.user = &user
	s.user.GenerateHashId()
	return s.user
}