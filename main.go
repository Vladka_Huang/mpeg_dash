package main

import (
	"fmt"
	"mpeg/database"
	"mpeg/database/migrations"
	"mpeg/grpc_client"
	"mpeg/redis"
	"mpeg/routes"
	"mpeg/websocket"
)

func main() {
	if err := database.Open(); err != nil {
		panic(fmt.Errorf("Fatal error connect database: %s\n", err))
	}
	defer database.Close()
	migrations.Migrate()

	redis.CreatePool()
	grpc_client.Dial()
	defer grpc_client.CloseConn()
	websocket.New()
	websocket.Dail()
	routes.RegisterApi()
}