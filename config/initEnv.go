package config

import (
	"fmt"
	"github.com/spf13/viper"
)

type dbConfig struct {
	Driver string
	Host string
	Port int
	User string
	Password string
	Database string
}

type redisConfig struct {
	Host            string
	Port            string
	PoolMaxIdle     int `mapstructure:"pool_max_idle"`
	PoolMaxActive   int `mapstructure:"pool_max_active"`
	PoolIdleTimeout int `mapstructure:"pool_idle_timeout"`
}

type HashConf struct {
	User string `mapstructure:"user"`
	Video string `mapstructure:"video"`
	Comment string `mapstructure:"comment"`
}

var DbSetting *dbConfig
var RedisConfig *redisConfig
var HashConfs *HashConf

func init() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.SetConfigType("json")
	if err := viper.ReadInConfig(); err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}

func LoadDb() {
	if err := viper.UnmarshalKey("database", &DbSetting); err != nil {
		panic("Unable to unmarshal database config")
	}
}

func LoadRedis() {
	if err := viper.UnmarshalKey("redis", &RedisConfig); err != nil {
		panic("Unable to unmarshal redisConfig")
	}
}

func LoadHashIds() {
	if err := viper.UnmarshalKey("hashids", &HashConfs); err != nil {
		panic("Unable to unmarshal hashids config")
	}
}