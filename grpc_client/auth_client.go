package grpc_client

import (
	"context"
	"google.golang.org/grpc"
	pb "mpeg/pb"
	"time"
)

type AuthClient struct {
	service  pb.AuthServiceClient
	serverName string
}

func NewAuthClient(cc *grpc.ClientConn, serverName string) *AuthClient {
	service := pb.NewAuthServiceClient(cc)
	return &AuthClient{service, serverName}
}

func (client *AuthClient) Login() (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	req := &pb.LoginRequest{
		ServerName: client.serverName,
	}

	res, err := client.service.Login(ctx, req)
	if err != nil {
		return "", err
	}

	return res.GetAccessToken(), nil
}