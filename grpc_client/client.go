package grpc_client

import (
	"bufio"
	"context"
	"google.golang.org/grpc"
	"io"
	"log"
	"mime/multipart"
	pb "mpeg/pb"
	"path/filepath"
	"time"
)

var Client pb.MpegClient
var conn *grpc.ClientConn
var conn2 *grpc.ClientConn

func UploadVideo(mpegClient pb.MpegClient, file multipart.File, fileName string, userId string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15 * time.Minute)
	defer cancel()

	stream, err := mpegClient.UploadVideo(ctx)
	if err != nil {
		log.Fatal("cannot upload video: ", err)
		return "", err
	}

	req := &pb.UploadVideoRequest{
		Data: &pb.UploadVideoRequest_Info{
			Info:	&pb.VideoInfo{
				Name: filepath.Base(fileName),
				Ext:  filepath.Ext(fileName),
				UserId: userId,
			},
		},
	}

	if err := stream.Send(req); err != nil {
		log.Fatal("cannot send video info to server: ", err, stream.RecvMsg(nil))
		return "", err
	}

	reader := bufio.NewReader(file)
	buffer := make([]byte, 1024)

	for {
		n, err := reader.Read(buffer)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal("cannot read chunk to buffer: ", err)
			return "", err
		}

		req := &pb.UploadVideoRequest{
			Data: &pb.UploadVideoRequest_ChunkData{
				ChunkData: buffer[:n],
			},
		}

		err = stream.Send(req)
		if err != nil {
			log.Fatal("cannot send chunk to server: ", err, stream.RecvMsg(nil))
			return "", err
		}
	}

	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatal("cannot receive response: ", err)
		return "", err
	}

	log.Printf("video uploaded with id : %s, size : %d", res.GetHashId(), res.GetSize())
	return res.GetHashId(), nil
}

func ServeVideo(mpegClient pb.MpegClient, path string, dataChan chan<- *pb.ServeVideoResponse) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1 * time.Minute)
	defer cancel()
	req := &pb.ServeVideoRequest{Path: path}
	stream, err := mpegClient.ServeVideo(ctx, req)
	if err != nil {
		log.Println("cannot get videos: ", err)
		return err
	}

	for {
		serveFile, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("%v.ServeVideio: %v", mpegClient, err)
			return err
		}
		dataChan <- serveFile
	}
	close(dataChan)
	return nil
}

func authMethods() map[string]bool {
	const mpegServicePath = "/mpeg.Mpeg/"

	return map[string]bool{
		mpegServicePath + "ServeVideo": true,
		mpegServicePath + "UploadVideo":  true,
	}
}

func Dial() {
	refreshDuration := 24 * 30 * time.Hour
	serverAddress := "localhost:3000"
	var err error

	conn, err = grpc.Dial(serverAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalln("cannot dail server: ", err)
		return
	}
	authClient := NewAuthClient(conn, "ap")
	interceptor, err := NewAuthInterceptor(authClient, authMethods(), refreshDuration)
	if err != nil {
		log.Fatal("cannot create auth interceptor: ", err)
	}

	conn2, err = grpc.Dial(
		serverAddress,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(interceptor.Unary()),
		grpc.WithStreamInterceptor(interceptor.Stream()),
	)
	if err != nil {
		log.Fatal("cannot dial server: ", err)
	}
	Client = pb.NewMpegClient(conn2)
}

func CloseConn() {
	_ = conn.Close()
	_ = conn2.Close()
}