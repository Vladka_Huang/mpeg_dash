package routes

import (
	"github.com/gin-contrib/size"
	"github.com/gin-gonic/gin"
	"golang.org/x/time/rate"
	"log"
	"mpeg/app/controllers"
	"mpeg/app/middlewares"
	"mpeg/websocket"
	"net/http"
	"time"
)

func RegisterApi() {
	rateLimit := rate.NewLimiter(rate.Every(100 * time.Millisecond), 5)
	engine := gin.Default()
	engine.Use(limits.RequestSizeLimiter(2 << 30))
	engine.Static("/js", "./js")
	engine.Static("/src", "./views/src")
	engine.LoadHTMLGlob("views/*.html")

	engine.GET("/", GetIndex)
	engine.GET("/player", GetPlayer)

	userRouter := engine.Group("/user")
	{
		userRouter.GET("/list", controllers.GetUserList)
		userRouter.POST("/create", controllers.Create)
		userRouter.POST("/login", controllers.Login)
		userRouter.GET("/info", middlewares.UserAuth(), controllers.GetInfo)
		userRouter.GET("/logout", middlewares.UserAuth(), controllers.Logout)
	}

	videoRouter := engine.Group("/video")
	{
		videoRouter.POST("/upload", middlewares.UserAuth(), controllers.UploadVideo)
		videoRouter.GET("/uploaded", middlewares.UserAuth(), controllers.GetUploadedVideos)
		videoRouter.GET("/list", controllers.GetVideoList)
		videoRouter.GET("/path/:videoId", controllers.GetPath)
		videoRouter.POST("/comment/:videoId", middlewares.RateAllow(rateLimit), controllers.CreateComment)
		videoRouter.GET("/comment/:videoId", controllers.GetComments)
		videoRouter.PUT("/done", controllers.Formatted)
		videoRouter.GET("/stream/:videoId/:segment", controllers.Stream)
	}

	engine.GET("/ws", func(c *gin.Context) {
		if err := websocket.Ws.Melody.HandleRequest(c.Writer, c.Request); err != nil {
			log.Println(err)
		}
	})

	if err := engine.RunTLS(":8080", "./certificate/cert.pem", "./certificate/key.pem"); err != nil {
		panic(err)
	}
}

func GetIndex(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "main.html", nil)
}

func GetPlayer(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "basicPlayer.html", nil)
}