package video_system

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"mpeg/grpc_server/grpc_config"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"sync"
	"time"
)

type VideoStore interface {
	Save(userId string, name string, ext string, videoData bytes.Buffer) (string, error)
}

type DiskVideoStore struct {
	mutex  sync.RWMutex
	dir    string
	videos map[string]*VideoInfo
}

type VideoInfo struct {
	Name string
	Ext  string
	Path string
	Id   uuid.UUID
}

func NewDiskVideoStore(dir string) *DiskVideoStore {
	return &DiskVideoStore{
		dir:    dir,
		videos: make(map[string]*VideoInfo),
	}
}

func (store *DiskVideoStore) Save(userId string, name string, ext string, videoData bytes.Buffer) (string, error) {
	videoId, err := uuid.NewRandom()
	if err != nil {
		return "", status.Errorf(codes.Unknown, fmt.Sprintf("generate uuid error: %v", err))
	}

	videoPath := fmt.Sprintf("%s%s/%s%s", store.dir, userId, videoId, ext)
	log.Println("path: ", videoPath)

	file, err := os.Create(videoPath)
	if err != nil {
		return "", status.Errorf(codes.Unknown, fmt.Sprintf("create file error: %v", err))
	}

	if _, err := videoData.WriteTo(file); err != nil {
		return "", status.Errorf(codes.Unknown, fmt.Sprintf("write file error: %v", err))
	}

	store.mutex.Lock()
	defer store.mutex.Unlock()

	video := &VideoInfo{
		Name: name,
		Ext:  ext,
		Path: videoPath,
		Id:   videoId,
	}
	store.videos[videoId.String()] = video
	JobChan <- video

	return videoId.String(), nil
}

var JobChan = make(chan *VideoInfo)
var DoneChan = make(chan uuid.UUID)

func FfmpegToDash() {
	for video := range JobChan {
		fmt.Println("consumer:", *video)
		videoIn := video.Path
		dstPath := filepath.Dir(videoIn) + "/" + video.Id.String() + "/dash/"
		if _, err := os.Stat(dstPath); os.IsNotExist(err) {
			if err = os.MkdirAll(dstPath, 0755); err != nil {
				log.Println(err)
				continue
			}
		}

		preset := "ultrafast"
		gopSize := 100
		fps := 25
		params := []string{
			"-i",
			videoIn,
			"-f",
			strconv.Itoa(fps),
			"-g",
			strconv.Itoa(gopSize),
			"-p",
			preset,
			"-o",
			dstPath,
		}
		cmd := exec.Command("./dash.sh", params...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			log.Println(err)
		}
		fmt.Println(string(out))
		DoneChan <- video.Id
	}
}

func NotifyAP() {
	timeout := 10 * time.Second
	client := http.Client{
		Timeout: timeout,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}
	for name := range DoneChan {
		fmt.Println("done: ", name)
		request, err := http.NewRequest("PUT", grpc_config.ApHost+"video/done?name="+name.String(), nil)

		if err != nil {
			log.Fatalln(err)
		}

		resp, err := client.Do(request)
		if err != nil {
			log.Fatalln(err)
		}
		resp.Body.Close()
	}
}

