package auth_service

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"log"
	"mpeg/jwt"
)

type AuthInterceptor struct {
	accessible map[string]bool
}

func NewAuthInterceptor(accessible map[string]bool) *AuthInterceptor {
	return &AuthInterceptor{accessible}
}

func (interceptor *AuthInterceptor) Unary(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (interface{}, error) {
	log.Println("--> unary interceptor: ", info.FullMethod)
	err := interceptor.Authorize(ctx, info.FullMethod)
	if err != nil {
		return nil, err
	}

	return handler(ctx, req)
}

func (interceptor *AuthInterceptor) Stream(
	srv interface{},
	stream grpc.ServerStream,
	info *grpc.StreamServerInfo,
	handler grpc.StreamHandler,
) error {
	log.Println("--> stream interceptor: ", info.FullMethod)
	err := interceptor.Authorize(stream.Context(), info.FullMethod)
	if err != nil {
		return err
	}

	return handler(srv, stream)
}

func (interceptor *AuthInterceptor) Authorize(ctx context.Context, method string) error {
	_, ok := interceptor.accessible[method]
	if !ok {
		return nil
	}

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return status.Errorf(codes.Unauthenticated, "metadata is not provided")
	}

	values := md["authorization"]
	if len(values) == 0 {
		return status.Errorf(codes.Unauthenticated, "authorization token is not provided")
	}

	accessToken := values[0]
	token, claims, err := jwt.ParseServerToken(accessToken)
	if err != nil || !token.Valid{
		return status.Errorf(codes.Unauthenticated, "access token is invalid: %v", err)
	}

	serverName := claims.ServerName
	if _, ok := AcceptServers[serverName]; !ok {
		return status.Error(codes.PermissionDenied, "no permission to access this RPC")
	}

	return nil
}