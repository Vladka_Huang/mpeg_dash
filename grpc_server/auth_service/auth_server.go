package auth_service

import (
	"context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"mpeg/jwt"
	pb "mpeg/pb"
)

type AuthServiceServer struct {
	pb.UnimplementedAuthServiceServer
}

func NewAuthServiceServer() *AuthServiceServer {
	return &AuthServiceServer{}
}

type Servers map[string]bool

var AcceptServers = Servers{
	"ap": true,
}

func (authServiceServer *AuthServiceServer)Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	serverName := req.GetServerName()
	if _, ok := AcceptServers[serverName]; !ok {
		return nil, status.Errorf(codes.Internal, "server unaccepted")
	}

	token, err := jwt.ServerTokenGenerate(serverName)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "cannot generate access token")
	}

	res := &pb.LoginResponse{AccessToken: token}
	return res, nil
}