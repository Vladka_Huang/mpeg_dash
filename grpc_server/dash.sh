#!/bin/bash

while getopts "i:f:g:p:o:" argv
do
    case $argv in
        i)
            VIDEO_IN=$OPTARG;;
        f)
            FPS=$OPTARG;;
        g)
            GOP_SIZE=$OPTARG;;
        p)
            PRESET_P=$OPTARG;;
        o)
            OUTPUT_PATH=$OPTARG;;
    esac
done

V_SIZE_1=416x234
V_SIZE_2=640x360
V_SIZE_3=1280x720

ffmpeg -i $VIDEO_IN -y \
    -preset $PRESET_P -keyint_min $GOP_SIZE -g $GOP_SIZE -sc_threshold 0 -r $FPS -c:v libx264 -pix_fmt yuv420p -c:a aac -b:a 128k -ac 1 -ar 44100 \
    -map v:0 -s:1 $V_SIZE_1 -b:v:1 145k -maxrate:1 155k -bufsize:1 220k \
    -map v:0 -s:2 $V_SIZE_2 -b:v:2 365k -maxrate:2 390k -bufsize:2 640k \
    -map v:0 -s:3 $V_SIZE_3 -b:v:3 3M -maxrate:5 3.21M -bufsize:3 5.5M \
    -map v:0 -s:4 $V_SIZE_3 -b:v:4 4.5M -maxrate:6 4.8M -bufsize:4 8M \
    -map 0:a \
    -init_seg_name init\$RepresentationID\$.\$ext\$ -media_seg_name chunk\$RepresentationID\$-\$Number%05d\$.\$ext\$ \
    -use_template 1 -use_timeline 1  \
    -seg_duration 4 -adaptation_sets "id=0,streams=v id=1,streams=a" \
    -f dash $OUTPUT_PATH/dash.mpd