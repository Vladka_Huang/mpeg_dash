package grpc_config

import (
	"fmt"
	"github.com/spf13/viper"
)

var ApHost string
var VideoPath string

func init() {
	viper.SetConfigName("grpc_server_config")
	viper.AddConfigPath("../")
	viper.SetConfigType("json")
	if err := viper.ReadInConfig(); err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}

func Load() {
	ApHost = viper.Get("ap_host").(string)
	VideoPath = viper.Get("video_path").(string)
}