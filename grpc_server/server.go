package main

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"mpeg/grpc_server/auth_service"
	"mpeg/grpc_server/grpc_config"
	"mpeg/grpc_server/mpeg_server"
	"mpeg/grpc_server/video_system"
	pb "mpeg/pb"
	"net"
)

func accessible() map[string]bool {
	const mpegServicePath = "/mpeg.Mpeg/"

	return map[string]bool{
		mpegServicePath + "ServeVideo": true,
		mpegServicePath + "UploadVideo":  true,
	}
}

func main() {
	log.Println("Start proto server")
	grpc_config.Load()

	videoStore := video_system.NewDiskVideoStore(grpc_config.VideoPath)
	mpegServer := mpeg_server.NewMpegServer(videoStore)
	authServiceServer := auth_service.NewAuthServiceServer()

	go func() {
		for i := 0; i < 4; i++ {
			go video_system.FfmpegToDash()
		}
	}()

	go func () {
		go video_system.NotifyAP()
	}()

	interceptor := auth_service.NewAuthInterceptor(accessible())
    server := grpc.NewServer(
    	grpc.UnaryInterceptor(interceptor.Unary),
    	grpc.StreamInterceptor(interceptor.Stream),
    )
    pb.RegisterMpegServer(server, mpegServer)
    pb.RegisterAuthServiceServer(server, authServiceServer)
    reflection.Register(server)

	address, err := net.Listen("tcp", ":3000")
	if err != nil {
		panic(err)
	}

	if err := server.Serve(address); err != nil {
		panic(err)
	}
}

