package mpeg_server

import (
	"bufio"
	"bytes"
	"fmt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"mpeg/grpc_server/grpc_config"
	"mpeg/grpc_server/video_system"
	pb "mpeg/pb"
	"os"
	"path/filepath"
)

const maxVideoSize = 2 << 30

type MpegServer struct {
	pb.UnimplementedMpegServer
	videoStore video_system.VideoStore
}

func NewMpegServer(videoStore video_system.VideoStore) *MpegServer {
	return &MpegServer{videoStore: videoStore}
}

func (mpegServer *MpegServer) UploadVideo(stream pb.Mpeg_UploadVideoServer) error {
	req, err := stream.Recv()
	if err != nil {
		return err
	}

	name := req.GetInfo().GetName()
	ext := req.GetInfo().GetExt()
	userId := req.GetInfo().GetUserId()
	log.Printf("receive an upload-video request: %s", name)

	videoData := bytes.Buffer{}
	videoSize := 0

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return status.Errorf(codes.Unknown, fmt.Sprintf("receive chunk data error: %v", err))
		}

		chunk := req.GetChunkData()
		size := len(chunk)

		videoSize += size
		if videoSize > maxVideoSize {
			return status.Errorf(codes.OutOfRange, fmt.Sprintf("video is too large: %d", videoSize))
		}
		if _, err := videoData.Write(chunk); err != nil {
			return status.Errorf(codes.FailedPrecondition, fmt.Sprintf("write chunk error: %v", err))
		}
	}

	videoId, err := mpegServer.videoStore.Save(userId, name, ext, videoData)
	if err != nil {
		return status.Errorf(codes.Internal, fmt.Sprintf("save video error: %v", err))
	}

	res := &pb.UploadVideoResponse{
		HashId: videoId,
		Size:   uint64(videoSize),
	}

	if err := stream.SendAndClose(res); err != nil {
		return status.Errorf(codes.Unknown, fmt.Sprintf("send response error: %v", err))
	}

	log.Printf("saved video with id: %s, size: %d", videoId, videoSize)
	return nil
}

func (mpegServer *MpegServer) ServeVideo(req *pb.ServeVideoRequest, stream pb.Mpeg_ServeVideoServer) error {
	var serveFile pb.ServeFile
	part := make([]byte, 1024)

	fileName := filepath.Base(req.Path)

	path := grpc_config.VideoPath + req.Path
	f, err := os.Open(path)
	if err != nil {
		log.Println(err)
	}
	r := bufio.NewReader(f)

	for {
		num, err := r.Read(part)

		if err == io.EOF {
			_ = f.Close()
			break
		}

		if err != nil {
			fmt.Println("Error reading file:", err)
			break
		}

		serveFile = pb.ServeFile{
			FileName: fileName,
			FileData: part[:num],
		}
		if err := stream.Send(&pb.ServeVideoResponse{File: &serveFile}); err != nil {
			return err
		}
	}
	return nil
}

