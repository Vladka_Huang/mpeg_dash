(function () {
    document.addEventListener("DOMContentLoaded", init);

    async function init() {
        const videosContainer = document.querySelector("#videos");
        const paginationContainer = document.querySelector("#pagination");
        paginationContainer.addEventListener("click", onPageChanged);

        let url = "https://localhost:8080/video/list?page=1";

        let data = await fetchData();
        let videos = data.records;
        let pages = getPages();

        makeListThroughFragment();
        makePaginationThroughFragment();

        function getPages() {
            return Array.from({ length: data.total_page }, (_, i) => {
                return i + 1 === data.page;
            });
        }

        async function onPageChanged(event) {
            url = "https://localhost:8080/video/list?page=" + event.target.value;
            data = await fetchData();
            videos = data.records;
            pages = getPages();
            makeListThroughFragment();
            makePaginationThroughFragment();
        }

        // methods
        function fetchData() {
            return new Promise((resolve, reject) => {
                fetch(url, {
                    method: "GET"
                })
                    .then((response) => {
                        return response.json();
                    })
                    .then((data) => {
                        console.log(data.data);
                        resolve(data.data);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        }

        function makeListThroughFragment() {
            const htmlFragement = document.createDocumentFragment();
            videos.forEach((video) => {
                const videoDiv = document.createElement("div");
                const videoLink = document.createElement("a");
                videoDiv.classList.add("video");
                videoLink.href = "https://localhost:8080/player?videoId=" + video.Id;
                // videoLink.target = '_blank'
                videoLink.textContent = video.Title;
                videoDiv.appendChild(videoLink);
                htmlFragement.appendChild(videoDiv);
            });
            videosContainer.innerHTML = "";
            videosContainer.appendChild(htmlFragement);
        }

        function makePaginationThroughFragment() {
            const htmlFragement = document.createDocumentFragment();
            const pageDiv = document.createElement("div");
            pages.forEach((page, index) => {
                const pageButton = document.createElement("button");
                pageButton.classList.add("perpage");
                if (page) {
                    pageButton.classList.add("active");
                }
                pageButton.value = index + 1;
                pageButton.textContent = index + 1;
                pageDiv.appendChild(pageButton);
                htmlFragement.appendChild(pageButton);
            });

            paginationContainer.innerHTML = "";
            paginationContainer.appendChild(htmlFragement);
        }
    }
})();
