(function () {
    let pageId = new URL(location.href).searchParams.get('videoId');

    const url = "../video/stream/" + pageId + "/mpd";
    const player = dashjs.MediaPlayer().create();
    player.initialize(document.querySelector("#videoPlayer"), url, true);

    document.addEventListener('DOMContentLoaded', initPlayer);

    async function initPlayer() {
        const chatContainer = document.querySelector("#chat > ul");

        let url = "wss://" + window.location.host + "/ws?room=" + pageId;
        let ws = new ReconnectingWebSocket(url);
        let vid = document.getElementById("videoPlayer");
        let text = document.getElementById("text");
        let input_name = document.getElementById("name")
        let output = document.getElementById("status");

        let data = await fetchComments();

        makeCommentsThroughFragment();

        var printStatus = function (status) {
            var d = document.createElement("div");
            d.innerHTML = status;
            output.appendChild(d);
        };

        var now = function () {
            var iso = new Date().toISOString();
            return iso.split("T")[1].split(".")[0];
        };

        function fetchComments() {
            const commentUrl = "https://localhost:8080/video/comment/" + pageId;
            return new Promise(((resolve, reject) => {
                fetch(commentUrl, {
                    method: "GET"
                })
                    .then((response) => {
                        return response.json();
                    })
                    .then((data) => {
                        console.log(data.data);
                        resolve(data.data);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            }));
        }

        function makeMsgItem(textStartAt, textComment, textName) {
            return `
        <li>
          <div class="msgLeft">
            <span class="start_at">${textStartAt}</span>
            <span class="comment">${textComment}</span>
          </div>
          <div class="msgRight">
            <span class="name">${textName}</span>
          </div>
        </li>
      `;
        }

        function makeCommentsThroughFragment() {
            let msgItemTemplate = "";
            data.forEach((comment) => {
                msgItemTemplate += makeMsgItem(
                    comment.start,
                    comment.title,
                    comment.name
                );
            });
            chatContainer.innerHTML = msgItemTemplate;
        }


        ws.onmessage = function (evt) {
            console.log("evt: ", evt)
            var msg = JSON.parse(evt.data);
            if (msg.name) {
                var time = new Date(msg.start * 1000).toISOString().substr(11, 8);
                var line = time + " " + msg.name + ": " + msg.content + "\n";
            }
            if (line) {
                chat.innerText += line;
            }
        };

        ws.onopen = function (evt) {
            console.log("ws connect")
            printStatus(now() + ' ' + '<span style="color: green;">Connected</span>');
        }

        ws.onclose = function (evt) {
            console.log("ws disconnect")
            printStatus(now() + ' ' + '<span style="color: red;">Disconnected</span>');
            ws = null;
        }

        text.onkeydown = function (e) {
            if (e.keyCode === 13 && text.value !== "") {
                let commentUrl = 'https://localhost:8080/video/comment/' + pageId;
                fetch(commentUrl, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        name: input_name.value,
                        content: text.value,
                        start: vid.currentTime
                    })
                }).then((response) => {
                    console.log(response);
                    return response.json();
                }).catch((err) => {
                    console.log('錯誤:', err);
                });
                text.value = "";
            }
        };
    }
})();